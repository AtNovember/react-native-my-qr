/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  // StyleSheet,
  // Text,
  // View
  BackHandler
} from 'react-native';
import { Provider } from "react-redux";
import { Actions } from "react-native-router-flux";
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

// import Camera from 'react-native-camera'
// import CameraQrScreen from './components/CameraQrScreen';
import reducers from './src/reducers';
import rootSaga from './src/sagas';

import Router from "./src/routes";

const sagaMiddleware = createSagaMiddleware();
const middlewares = [thunk, sagaMiddleware];
const store = createStore(reducers, applyMiddleware(...middlewares));
sagaMiddleware.run(rootSaga);

export default class App extends Component {
  static handleBackButtonClick() {
    Actions.pop();
    return true;
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', App.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', App.handleBackButtonClick);
  }

  render() {
    return (
      <Provider store={store}>
        <Router/>
      </Provider>
    );
  }
}
