import { INIT_USER, SET_PHONE, LOGIN_ERROR } from "../actions/user";

export const initialState = {
  phone: "",
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case INIT_USER:
      return action.payload;
    case LOGIN_ERROR: 
      return { ...state, error: action.payload };
    case SET_PHONE:
      return { ...state, phone: action.payload };
    default:
      return state;
  }
};
