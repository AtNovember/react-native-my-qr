import { ON_LOADING, OFF_LOADING } from '../actions/loading';

export const initialState = {
  login: false,
  createProfile: false,
  confirmProfile: false,
  resetPass: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ON_LOADING:
      return { ...state, [action.payload]: true };
    case OFF_LOADING:
      return { ...state, [action.payload]: false };
    default:
      return state;
  }
}