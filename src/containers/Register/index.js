import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Text, Keyboard, KeyboardAvoidingView } from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content,
  Grid,
  Item,
  Badge,
  Input
} from 'native-base';
// import { Col, Row, Grid } from 'react-native-easy-grid';
import { Row, Col } from 'react-native-easy-grid';
import { Actions } from 'react-native-router-flux';
import {
  createProfile,
  confirmProfile,
  setPhone /*, sendCode */
} from "../../actions/user";

import style, { input } from './style';
// import { createProfileFlow } from '../../sagas/user';

class Register extends Component {
  static propTypes = {
    createProfile: PropTypes.func.isRequired,
    confirmProfile: PropTypes.func.isRequired,
    setPhone: PropTypes.func.isRequired,
    sendCode: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired
  };

  getBack = () => {
    Actions.push("Welcome");
  };

  state = {
    name: "",
    phone: "",
    password: "",
    repeatPassword: "",
    code: "",
    isKeyboardOpen: false,
    expectForCode: false
  };

  componentDidMount() {
    this.keyboardDidShow = Keyboard.addListener(
      "keyboardDidShow",
      this.checkKeyboard
    );
    this.keyboardDidHide = Keyboard.addListener(
      "keyboardDidHide",
      this.checkKeyboard
    );
  }

  componentWillUnmount() {
    this.keyboardDidShow.remove();
    this.keyboardDidHide.remove();
    Keyboard.dismiss();
  }

  checkKeyboard = event => {
    if (event) {
      this.setState({
        isKeyboardOpen: true
      });
      return;
    }
    this.setState({
      isKeyboardOpen: false
    });
  };

  checkPassword = () => {
    const { password, repeatPassword } = this.state;
    return !(password === repeatPassword);
  }

  handleUpdateField = (field, value) => {
    this.setState({
      [field]: value
    });
  };

  handleFirstStep = () => {
    const { name, phone } = this.state;

    if (this.props.loading) {
      return;
    }
    this.props.createProfile({ name, phone });
    this.setState({ expectForCode: true });
  };

  handleSecondStep = () => {
    const { code, phone, password } = this.state;
    if (this.props.loading) {
      return;
    }
    this.props.confirmProfile({ code, phone, password });
  };

  render() {
    return (
      <Container style={style.screenWrapper}>
        <Header androidStatusBarColor="black" style={style.header}>
          <Left>
            <Button transparent onPress={this.getBack}>
              <Icon type="Ionicons" name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Регистрация</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="Ionicons" name="menu" />
            </Button>
          </Right>
        </Header>
        {this.state.expectForCode && (
          <Content
            style={style.content}
            centerContent
            disableKBDismissScroll={true}
          >
            <View
              style={
                this.state.isKeyboardOpen
                  ? style.formViewKeyboard
                  : style.formView
              }
            >
              <Item style={style.inputWrapper}>
                <Badge style={style.inputIconBadge}>
                  <Icon style={style.inputIcon} type="Feather" name="lock" />
                </Badge>
                <Input
                  {...input}
                  value={this.state.code}
                  onChangeText={value => this.handleUpdateField("code", value)}
                  placeholder="Код"
                  caretHidden={false}
                  keyboardType="numeric"
                />
              </Item>
            </View>
            <View style={style.buttonWrapper}>
              <Button style={style.button} onPress={this.handleSecondStep}>
                <Text style={style.buttonText}>Отправить</Text>
              </Button>
            </View>
          </Content>
        )}
        {!this.state.expectForCode && (
          <Content
            style={style.content}
            centerContent
            disableKBDismissScroll={true}
          >
            <View
              style={
                this.state.isKeyboardOpen
                  ? style.formViewKeyboard
                  : style.formView
              }
            >
              <Item style={style.inputWrapper}>
                <Badge style={style.inputIconBadge}>
                  <Icon style={style.inputIcon} type="Feather" name="user" />
                </Badge>
                <Input
                  {...input}
                  value={this.state.name}
                  onChangeText={value => this.handleUpdateField("name", value)}
                  placeholder="Имя"
                  caretHidden={false}
                />
              </Item>
              <Item style={style.inputWrapper}>
                <Badge style={style.inputIconBadge}>
                  <Icon style={style.inputIcon} type="Feather" name="phone" />
                </Badge>
                <Input
                  {...input}
                  value={this.state.phone}
                  onChangeText={value => this.handleUpdateField("phone", value)}
                  placeholder="Телефон"
                  caretHidden={false}
                  keyboardType="phone-pad"
                />
              </Item>
              <Item style={style.inputWrapper}>
                <Badge style={style.inputIconBadge}>
                  <Icon style={style.inputIcon} type="Feather" name="lock" />
                </Badge>
                <Input
                  {...input}
                  value={this.state.password}
                  onChangeText={value =>
                    this.handleUpdateField("password", value)
                  }
                  placeholder="Пароль"
                  caretHidden={false}
                  secureTextEntry={true}
                />
              </Item>
              <Item style={style.inputWrapper}>
                <Badge style={style.inputIconBadge}>
                  <Icon style={style.inputIcon} type="Feather" name="lock" />
                </Badge>
                <Input
                  {...input}
                  value={this.state.repeatPassword}
                  onChangeText={value =>
                    this.handleUpdateField("repeatPassword", value)
                  }
                  placeholder="Повторите Пароль"
                  caretHidden={false}
                  secureTextEntry={true}
                />
              </Item>
              {this.checkPassword() && <Text>Пароли не совпадают!</Text>}
            </View>
            <View style={style.buttonWrapper}>
              <Button style={style.button} onPress={this.handleFirstStep}>
                <Text style={style.buttonText}>Регистрация</Text>
              </Button>
            </View>
          </Content>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.loading.createProfile
  // confirmProfile: state.loading.confirmProfile
});

const mapDispatchToProps = (dispatch) => ({
  createProfile: (credentials) => dispatch(createProfile(credentials)),
  confirmProfile: (credentials) => dispatch(confirmProfile(credentials)),
  setPhone: phone => dispatch(setPhone(phone)),
  sendCode: code => dispatch(sendCode(code))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);