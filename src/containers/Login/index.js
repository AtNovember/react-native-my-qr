import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { View, Text, Keyboard } from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Content,
  Item,
  Badge,
  Icon,
  Input
} from "native-base";
import { Actions } from "react-native-router-flux";
import { login } from "../../actions/user";
import formatPhone from '../../components/FormatPhoneNumber'

import style, { input } from "./style";

class Login extends Component {
  static propTypes = {
    login: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
  };

  getBack = () => {
    Actions.push("Welcome");
  };

  state = {
    username: "",
    password: "",
    // error: "",
    isKeyboardOpen: false
  };

  componentWillUnmount() {
    this.keyboardDidShow.remove();
    this.keyboardDidHide.remove();
    Keyboard.dismiss();
  }

  checkKeyboard = event => {
    if (event) {
      this.setState({
        isKeyboardOpen: true
      });
      return;
    }
    this.setState({
      isKeyboardOpen: false
    });
  };

  handleUpdateField = (field, value) => {
    this.setState({
      [field]: value
    });
  };



  handleLogin = () => {
    const { username, password } = this.state;

    console.log("entered phone src", username);
    console.log("entered phone formatted", formatPhone(username));
    if (this.props.loading) {
      return;
    }
    // this.props.login({ username, password });
  };

  handleRestore = () => {
    Actions.push("Restore");
  };

  render() {
    return (
      <Container style={style.screenWrapper}>
        <Header androidStatusBarColor="black" style={style.header}>
          <Left>
            <Button transparent onPress={this.getBack}>
              <Icon type="Ionicons" name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Вход</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="Ionicons" name="menu" />
            </Button>
          </Right>
        </Header>
        <Content
          style={style.content}
          centerContent
          disableKBDismissScroll={true}
        >
          <View
            style={
              this.state.isKeyboardOpen
                ? style.formViewKeyboard
                : style.formView
            }
          >
            <Item style={style.inputWrapper}>
              <Badge style={style.inputIconBadge}>
                <Icon style={style.inputIcon} type="Feather" name="phone" />
              </Badge>
              <Input
                {...input}
                value={this.state.username}
                onChangeText={value =>
                  this.handleUpdateField("username", value)
                }
                placeholder="Телефон"
                caretHidden={false}
                keyboardType="phone-pad"
              />
            </Item>
            <Item style={style.inputWrapper}>
              <Badge style={style.inputIconBadge}>
                <Icon style={style.inputIcon} type="Feather" name="lock" />
              </Badge>
              <Input
                {...input}
                value={this.state.password}
                onChangeText={value =>
                  this.handleUpdateField("password", value)
                }
                placeholder="Пароль"
                caretHidden={false}
                secureTextEntry={true}
              />
            </Item>
            <Button
              transparent
              style={style.restoreButton}
              onPress={this.handleRestore}
            >
              <Text>Забыли пароль?</Text>
            </Button>
            {this.props.error && <Text>error{this.props.error}</Text>}
          </View>
          <View style={style.buttonWrapper}>
            <Button
              style={style.button}
              style={style.button}
              onPress={this.handleLogin}
            >
              <Text style={style.buttonText}>Вход</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.loading.login,
  error: state.user.error
});

const mapDispathcToProps = dispatch => ({
  login: credentials => dispatch(login(credentials))
});

export default connect(mapStateToProps, mapDispathcToProps)(Login);
