import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
  screenWrapper: {
    width,
    height,
    flex: 1,
    // backgroundColor: '#334',
    backgroundColor: '#111',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  header: {
    backgroundColor: '#000',
    height: 0
  },
  emptyWrapper: {
    // flex: 1,
    height: height * 0.01,
  },
  logoWrapper: {
    width: width,
    height: height * 0.5,
    alignItems: 'center'
    // flex: 1
  },
  logo: {
    resizeMode: 'contain',
    height: height * 0.35,
    width: width * 0.66
  },
  buttonWrapper: {
    width,
    // flex: 1,
    width: width * 0.88,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: 'yellow',
    width: '100%',
    marginBottom: width * (1 - 0.88) / 2,
    justifyContent: 'center'
  },
  buttonText: {
    color: 'black'
  }
})