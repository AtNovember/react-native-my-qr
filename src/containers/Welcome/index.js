import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { Header, Container, Content, Grid, Button, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import style from './style';

const Logo = require('../../assets/logo_en.png');

export default class Welcome extends Component {
  pushToLogin = () => {
    Actions.push('Login');
  }

  pushToRegister = () => {
    Actions.push('Register');
  }

  render() {
    return (
      <Container style={style.screenWrapper}>
        <Header androidStatusBarColor="black" style={style.header} />
        <View style={style.logoWrapper}>
          <Image source={Logo} style={style.logo} />
        </View>
        <View style={style.buttonWrapper}>
          <Button style={style.button} onPress={this.pushToLogin}>
            <Text style={style.buttonText}>Вход</Text>
          </Button>
          <Button style={style.button} onPress={this.pushToRegister}>
            <Text style={style.buttonText}>Регистрация</Text>
          </Button>
        </View>
      </Container>
    )
  }
}
