import React, { Component } from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { View, Text } from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content
} from "native-base";
import { Actions } from "react-native-router-flux";
import { logout } from "../../actions/user";

import style from "./style";


class MainScreen extends Component {
  static propTypes = {
    logout: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired
  };
  openQr = () => {
    Actions.push('QrCamera');
  }

  getBack = () => {
    // Action.push('L')
    // this.props.logout();
    Actions.replace("Login");
  }

  render() {
    return <Container style={style.screenWrapper}>
      <Header androidStatusBarColor="black" style={style.header}>
        <Left>
          <Button transparent onPress={this.getBack}>
            <Icon type="Ionicons" name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Выбор действия</Title>
        </Body>
        <Right>
          <Button transparent>
            <Icon type="Ionicons" name="menu" />
          </Button>
        </Right>
      </Header>
      <Content style={style.content} centerContent disableKBDismissScroll={true}>
        <View style={style.formViewKeyboard}>
          <Button style={style.button} onPress={this.openQr}>
            <Text style={style.buttonText}>Сканировать QR-код</Text>
          </Button>
          <View>
            <Text>NFC</Text>
          </View>
        </View>
      </Content>
    </Container>;
  }
}


const mapStateToProps = (state) => ({
  loading: state.loading.login
});

const mapDispathcToProps = (dispatch) => ({
  logout: () => dispatch(logout())
});

export default connect(mapStateToProps, mapDispathcToProps)(MainScreen);