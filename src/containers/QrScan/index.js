import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content
} from "native-base";
import { Actions } from "react-native-router-flux";

import CameraQrScreen from '../../components/CameraQrScreen';
import style from "./style";


export default class MainScreen extends Component {
  getBack = () => {
    Actions.push('MainScreen');
  }

  render() {
    return <Container style={style.screenWrapper}>
        <Header androidStatusBarColor="black" style={style.header}>
          <Left>
            <Button transparent onPress={this.getBack}>
              <Icon type="Ionicons" name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Qr-сканер</Title>
          </Body>
          <Right>
            <Button transparent>
              {/* <Icon type="Ionicons" name="menu" /> */}
            </Button>
          </Right>
        </Header>
        <CameraQrScreen></CameraQrScreen>
      </Container>;
  }
}