import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
const marginHoriz = width * (1 - 0.88) / 2;
const marginVertical = height * 0.01;

const form = {
  width,
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  alignContent: "center"
};

const style = StyleSheet.create({
  screenWrapper: {
    height,
    width,
    backgroundColor: "#111"
  },
  header: {
    backgroundColor: "#000",
    height: 50
  },
  content: {
    backgroundColor: "yellow",
    flexDirection: "column"
  },
  formView: {
    ...form,
    height: height - (height * 0.2 + 50)
  },
  formViewKeyboard: {
    ...form,
    height: height * 0.3
  },
  inputWrapper: {
    width: width * 0.88,
    borderWidth: 2,
    borderColor: "#273248"
  },
  inputStyle: {
    color: "black"
  },
  inputIconBadge: {
    width: 25,
    height: 25,
    left: 12.5,
    top: 12.5,
    marginRight: width * 0.1,
    backgroundColor: "#273248",
    justifyContent: "center"
  },
  inputIcon: {
    color: "white",
    fontSize: 12.5,
    lineHeight: 12.5
  },
  buttonWrapper: {
    height: height * 0.15,
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    justifyContent: "flex-end",
    alignContent: "center"
  },
  button: {
    backgroundColor: "#273248",
    width: width * 0.88,
    marginTop: 0,
    marginLeft: marginHoriz,
    marginRight: marginHoriz,
    marginBottom: marginHoriz,
    justifyContent: "center"
  },
  buttonText: {
    color: "white"
  }
});

export const input = {
  placeholderTextColor: "#998235",
  style: style.inputStyle
};

export default style;
