import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { View, Text, Keyboard } from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Content,
  Item,
  Badge,
  Icon,
  Input
} from "native-base";
import { Actions } from "react-native-router-flux";
import { resetPass, updatePass } from "../../actions/user";
import style, { input } from "./style";

class Restore extends Component {
  static propTypes = {
    resetPass: PropTypes.func.isRequired,
    updatePass: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired
  };

  getBack = () => {
    Actions.push("Login");
  };

  state = {
    phone: "",
    code: "",
    password: "", // новый пароль
    isKeyboardOpen: false,
    step1: true,
    step2: false
  };

  componentWillUnmount() {
    this.keyboardDidShow.remove();
    this.keyboardDidHide.remove();
    Keyboard.dismiss();
  }

  checkKeyboard = event => {
    if (event) {
      this.setState({
        isKeyboardOpen: true
      });
      return;
    }
    this.setState({
      isKeyboardOpen: false
    });
  };

  handleUpdateField = (field, value) => {
    this.setState({
      [field]: value
    });
  };

  handleResetPass = () => {
    const { phone } = this.state;
    console.log("reset pass for number", phone);

    if (this.props.loading) {
      return;
    }
    this.props.resetPass({ phone });
    this.setState({ step1: false, step2: true });
  };

  handleUpdatePass = () => {
    const { phone, code, password } = this.state;
    console.log("newPass", { phone, code, password });
    if (this.props.loading) {
      return;
    }
    this.props.updatePass({ phone, code, password });
    // this.setState({ step1: false, step2: true });
  };

  render() {
    return (
      <Container>
        <Header androidStatusBarColor="black" style={style.header}>
          <Left>
            <Button transparent onPress={this.getBack}>
              <Icon type="Ionicons" name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Восстановление пароля</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="Ionicons" name="menu" />
            </Button>
          </Right>
        </Header>
        {this.state.step1 && (
          <Content
            style={style.content}
            centerContent
            disableKBDismissScroll={true}
          >
            <View
              style={
                this.state.isKeyboardOpen
                  ? style.formViewKeyboard
                  : style.formView
              }
            >
              <Text>Укажите номер телефона</Text>
              <Text>для которого хотите сбросить пароль</Text>
              <Item style={style.inputWrapper}>
                <Badge style={style.inputIconBadge}>
                  <Icon style={style.inputIcon} type="Feather" name="phone" />
                </Badge>
                <Input
                  {...input}
                  value={this.state.phone}
                  onChangeText={value => this.handleUpdateField("phone", value)}
                  placeholder="Телефон"
                  caretHidden={false}
                  keyboardType="phone-pad"
                />
              </Item>
            </View>
            <View style={style.buttonWrapper}>
              <Button
                style={style.button}
                style={style.button}
                onPress={this.handleResetPass}
              >
                <Text style={style.buttonText}>Сбросить пароль</Text>
              </Button>
            </View>
          </Content>
        )}
        {this.state.step2 && (
          <Content
            style={style.content}
            centerContent
            disableKBDismissScroll={true}
          >
            <View
              style={
                this.state.isKeyboardOpen
                  ? style.formViewKeyboard
                  : style.formView
              }
            >
              <Text>Ваш номер телефона {this.state.pnone}</Text>
              <Text />
              <Item style={style.inputWrapper}>
                <Badge style={style.inputIconBadge}>
                  <Icon style={style.inputIcon} type="Feather" name="lock" />
                </Badge>
                <Input
                  {...input}
                  value={this.state.code}
                  onChangeText={value => this.handleUpdateField("code", value)}
                  placeholder="Код"
                  caretHidden={false}
                  keyboardType="numeric"
                />
              </Item>
              <Item style={style.inputWrapper}>
                <Badge style={style.inputIconBadge}>
                  <Icon style={style.inputIcon} type="Feather" name="lock" />
                </Badge>
                <Input
                  {...input}
                  value={this.state.password}
                  onChangeText={value =>
                    this.handleUpdateField("password", value)
                  }
                  placeholder="Пароль"
                  caretHidden={false}
                  secureTextEntry={true}
                />
              </Item>
            </View>
            <View style={style.buttonWrapper}>
              <Button
                style={style.button}
                style={style.button}
                onPress={this.handleUpdatePass}
              >
                <Text style={style.buttonText}>Изменить пароль</Text>
              </Button>
            </View>
          </Content>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.loading.resetPass
  // confirmProfile: state.loading.confirmProfile
});

const mapDispatchToProps = dispatch => ({
  resetPass: phone => dispatch(resetPass(phone)),
  updatePass: credentials => dispatch(updatePass(credentials))
});

export default connect(mapStateToProps, mapDispatchToProps)(Restore);
