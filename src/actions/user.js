export const INIT_USER = "actions/user/INIT_USER";

export const LOGIN = "actions/user/LOGIN";
export const LOGIN_SUCCESS = "actions/user/LOGIN_SUCCESS";
export const LOGIN_ERROR = "actions/user/LOGIN_ERROR";

export const LOGOUT = "actions/user/LOGOUT";

export const GET_PROFILE = "actions/user/GET_PROFILE";
export const GET_PROFILE_SUCCESS = "actions/user/GET_PROFILE_SUCCESS";
export const GET_PROFILE_ERROR = "actions/user/GET_PROFILE_ERROR";

export const CREATE_PROFILE = "actions/user/CREATE_PROFILE";
export const CREATE_PROFILE_SUCCESS = "actions/user/CREATE_PROFILE_SUCCESS";
export const CREATE_PROFILE_ERROR = "actions/user/CREATE_PROFILE_ERROR";

export const CONFIRM_PROFILE = "actions/user/CONFIRM_PROFILE";
export const CONFIRM_PROFILE_SUCCESS = "actions/user/CONFIRM_PROFILE_SUCCESS";
export const CONFIRM_PROFILE_ERROR = "actions/user/CONFIRM_PROFILE_ERROR";

export const REPEAT_REG_PROFILE = "actions/user/REPEAT_REG_PROFILE";
export const SET_PHONE = "actions/user/SET_PHONE";

export const RESET_PASSWORD = "actions/user/RESET_PASSWORD";
export const UPDATE_PASSWORD = "actions/user/UPDATE_PASSWORD";

export const initUser = user => ({
  type: LOGIN_SUCCESS,
  payload: user
});

export const login = credentials => ({
  type: LOGIN,
  payload: credentials
});
export const loginSuccess = () => ({
  type: LOGIN_SUCCESS
});
export const loginError = error => ({
  type: LOGIN_ERROR,
  error
});

export const getProfile = profileId => ({
  type: GET_PROFILE,
  payload: profileId
});
export const getProfileSuccess = () => ({
  type: GET_PROFILE_SUCCESS
});
export const getProfileError = error => ({
  type: GET_PROFILE_ERROR,
  error
});

export const createProfile = credentials => ({
  type: CREATE_PROFILE,
  payload: credentials
});

export const createProfileSuccess = () => ({
  type: CREATE_PROFILE_SUCCESS
});

export const createProfileError = error => ({
  type: CREATE_PROFILE_ERROR,
  error
});

export const confirmProfile = credentials => ({
  type: CONFIRM_PROFILE,
  payload: credentials
});

export const confirmProfileSuccess = () => ({
  type: CONFIRM_PROFILE_SUCCESS
});

export const confirmProfileError = error => ({
  type: CONFIRM_PROFILE_ERROR,
  error
});

// export const repeatRegProfile = phone => ({
//   type: REPEAT_REG_PROFILE,
//   payload: phone
// });

export const setPhone = phone => ({
  type: SET_PHONE,
  payload: phone
});

export const resetPass = phone => ({
  type: RESET_PASSWORD,
  payload: phone
});

export const updatePass = credentials => ({
  type: UPDATE_PASSWORD,
  payload: credentials
});

export const logout = () => ({
  type: LOGOUT
});
