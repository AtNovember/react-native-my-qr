export const ON_LOADING = "actions/app/ON_LOADING";
export const OFF_LOADING = "actions/app/OFF_LOADING";

export const onLoading = loading => ({
  type: ON_LOADING,
  payload: loading
});

export const offLoading = loading => ({
  type: OFF_LOADING,
  payload: loading
});
