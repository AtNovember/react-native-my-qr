import { AsyncStorage } from "react-native";
import Model from "./Model";

export default class Profile extends Model {
  plural = "Profiles";

  async login(credentials) {
    const result = await this.postRequest(`${this.plural}/login`, credentials);
    this.setStorage(result);
    return result;
  }

  async setStorage(data) {
    await AsyncStorage.setItem("tokenId", data.id);
    await AsyncStorage.setItem("userId", data.userId);
  }

  getMyProfile() {
    return this.getRequest(`${this.plural}/my-profile`);
  }

  async createProfile(credentials) {
    const result = await this.postRequest(`${this.plural}/regStep1`, credentials);
    return result;
  }

  async confirmProfile(credentials) {
    await this.postRequest(`${this.plural}/regStep2`, credentials);
  }

  async resetPass(phone) {
    await this.postRequest(`${this.plural}/resetPass`, phone);
  }

  // POST /profiles/checkCodeResetPassword
  async updatePass(credentials) {
    await this.postRequest(`${this.plural}/checkCodeResetPassword`, credentials);
  }

  logout() {
    return this.postRequest(`${this.plural}/logout`).then(() => {
      AsyncStorage.removeItem("tokenId");
      AsyncStorage.removeItem("userId");
    });
  }
}
