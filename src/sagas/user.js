import { fork, put, call, takeEvery } from "redux-saga/effects";
import { Actions } from "react-native-router-flux";

import ProfilesApi from "../api/Profiles";
import * as loadingActions from "../actions/loading";
import * as userActions from "../actions/user";

export function* login(action) {
  let success = true;
  let user = {};
  try {
    yield put(loadingActions.onLoading("login"));
    const { payload: credentials } = action;
    const Profiles = new ProfilesApi();
    // const user = yield call([Profiles, Profiles.login], credentials);
    user = yield call([Profiles, Profiles.login], credentials);
    console.log("response", user);
    // yield put(userActions.initUser(user.userInfo));
  } catch (error) {
    console.warn('error:', error);
    success = false;
    yield put(userActions.loginError(error));
  } finally {
    yield put(loadingActions.offLoading("login"));
  }

  if (success) {
    console.log('response user', user);
    yield put(userActions.initUser(user));
    // Actions.replace("MainScreen");
    Actions.push("MainScreen");
  }
}

export function* loginFlow() {
  yield takeEvery(userActions.LOGIN, login);
}

export function setPhone(action) {
  const { payload: phone } = action;
  put(userActions.setPhone, phone);
}

export function* createProfile(action) {
  try {
    yield put(loadingActions.onLoading("createProfile"));
    const { payload: credentials } = action;
    const { phone } = credentials;
    const Profiles = new ProfilesApi();
    const response = yield call([Profiles, Profiles.createProfile], credentials);
  } catch (error) {
    yield put(userActions.createProfileError());
  } finally {
    yield put(loadingActions.offLoading("createProfile"));
    // Actions.replace("Confirm");
  }
}

export function* createProfileFlow() {
  yield takeEvery(userActions.CREATE_PROFILE, createProfile);
}

export function* confirmProfile(action) {
  try {
    yield put(loadingActions.onLoading("createProfile"));
    const { payload: credentials } = action;
    const { phone } = credentials;
    yield put(userActions.setPhone(phone));
    const Profiles = new ProfilesApi();
    yield call([Profiles, Profiles.confirmProfile], credentials);
  } catch (error) {
    yield put(userActions.confirmProfileError());
  } finally {
    yield put(loadingActions.offLoading("createProfile"));
    Actions.replace("Login");
  }
}

export function* confirmProfileFlow() {
  yield takeEvery(userActions.CONFIRM_PROFILE, confirmProfile);
}

export function* resetPassword(action) {
  try {
    yield put(loadingActions.onLoading("resetPass"));
    const { payload: phone } = action;
    const Profiles = new ProfilesApi();
    yield call([Profiles, Profiles.resetPass], phone);
  } catch (error) {
    // yield put(userActions.confirmProfileError());
    console.log("resetPassword error", error);
  } finally {
    yield put(loadingActions.offLoading("resetPass"));
  }
}

export function* resetPasswordFlow() {
  yield takeEvery(userActions.RESET_PASSWORD, resetPassword);
}

export function* updatePassword(action) {
  try {
    yield put(loadingActions.onLoading("resetPass"));
    const { payload: credentials } = action;
    const Profiles = new ProfilesApi();
    yield call([Profiles, Profiles.updatePass], credentials);
  } catch (error) {
    // yield put(userActions.confirmProfileError());
    console.log("updatePassword error", error);
  } finally {
    yield put(loadingActions.offLoading("resetPass"));
    Actions.replace("Login");
  }
}

export function* updatePasswordFlow() {
  yield takeEvery(userActions.UPDATE_PASSWORD, updatePassword);
}

export function* repeatRegUser(action) {
  const { email } = action.payload;
  const Profiles = new ProfilesApi();
  yield call([Profiles, Profiles.repeatRegUser], email);
}

export function* repeatRegUserFlow() {
  yield takeEvery(userActions.REPEAT_REG_PROFILE, repeatRegUser);
}

export function* getProfile(action) {
  try {
    yield put(loadingActions.onLoading("gettingProfile"));

    const { payload: profileId } = action;
    const Profiles = new ProfilesApi();

    const user = yield call([Profiles, Profiles.getById], profileId);

    yield put(userActions.getProfileSuccess());
    yield put(userActions.initUser(user));
  } catch (error) {
    yield put(userActions.getProfileError());
  } finally {
    yield put(loadingActions.offLoading("gettingProfile"));
  }
}

export function* getProfileFlow() {
  yield takeEvery(userActions.GET_PROFILE, getProfile);
}

export function* logout() {
  const Profiles = new ProfilesApi();
  yield call([Profiles, Profiles.logout]);
}

export function* logoutFlow() {
  yield takeEvery(userActions.LOGOUT, logout);
}

export default function* () {
  yield [
    fork(getProfileFlow),
    fork(loginFlow),
    fork(createProfileFlow),
    fork(confirmProfileFlow),
    fork(repeatRegUserFlow),
    fork(resetPasswordFlow),
    fork(updatePasswordFlow),
    fork(logoutFlow)
  ];
}
