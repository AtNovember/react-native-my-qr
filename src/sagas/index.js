import userSaga from "./user";
// import startUpSaga from "./startUp";

export default function* () {
  yield [
    userSaga()
    // startUpSaga()
  ];
}
