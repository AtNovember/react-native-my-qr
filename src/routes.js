import React, { Component } from 'react';
import { Router, Scene, Lightbox } from 'react-native-router-flux';

import Welcome from './containers/Welcome';
import Login from './containers/Login';
import Register from './containers/Register';
import Restore from './containers/Restore';

import MainScreen from './containers/MainScreen';

// import QrCamera from './components/CameraQrScreen';
import QrCamera from './containers/QrScan';


export default class App extends Component {
  render() {
    return (
      <Router>
        <Lightbox>
          <Scene key="root">
            <Scene
              hideNavBar
              key='Welcome'
              initial
              component={Welcome}
            />
            <Scene
              hideNavBar
              key='Login'
              component={Login}
            />
            <Scene
              hideNavBar
              key='Register'
              component={Register}
            />
            <Scene
              hideNavBar
              key='Restore'
              component={Restore}
            />
            <Scene
              hideNavBar
              key='Restore'
              component={Restore}
            /> 
            <Scene
              hideNavBar
              key='MainScreen'
              component={MainScreen}
            />
            <Scene
              hideNavBar
              key='QrCamera'
              component={QrCamera}
            />

          </Scene>
        </Lightbox>
      </Router>
    )
  }
}