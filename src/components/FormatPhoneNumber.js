const formatPhone = (num) => {
  const str = num.toString();
  const matched = str.match(/\d+\.?\d*/g);

  if (matched.length === 1) {
    // 10 цифр:
    if (matched[0].length === 10) {
      return `7${matched[0].substr(0, 10)}`
    }
    // 11 цифр:
    if(matched[0].length === 11) {
      return `7${matched[0].substr(1,10)}`
    }
  }
  return num;
};

export default formatPhone;

