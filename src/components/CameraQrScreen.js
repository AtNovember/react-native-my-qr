import React, { Component } from 'react';
// import { Text, Toast } from "react-native";
import { Text, ToastAndroid } from "react-native";
import { QRScannerView } from 'ac-qrcode';

export default class CameraQrScreen extends Component {
  render() {
    return (
      <QRScannerView
        onScanResultReceived={this.barcodeReceived.bind(this)}

        renderTopBarView={() => this._renderTitleBar()}

        renderBottomMenuView={() => this._renderMenu()}
        hintText='hintText'
      />
    )
  }

  _renderTitleBar() {
    return (
      <Text
        style={{ color: 'white', textAlignVertical: 'center', textAlign: 'center', font: 20, padding: 12 }}
      >Here is title bar</Text>
    );
  }

  _renderMenu() {
    return (
      <Text
        style={{ color: 'white', textAlignVertical: 'center', textAlign: 'center', font: 20, padding: 12 }}
      >Here is bottom menu</Text>
    )
  }

  barcodeReceived(e) {
    ToastAndroid.show("Type: " + e.type + "\nData: " + e.data, ToastAndroid.SHORT);
    //console.log(e)
  }
}